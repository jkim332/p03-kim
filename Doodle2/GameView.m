//
//  GameView.m
//  Doodle2
//
//  Created by Patrick Madden on 2/4/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import "GameView.h"

@implementation GameView
@synthesize jumper, bricks;
@synthesize tilt;
NSInteger currentScore;

-(id)initWithCoder:(NSCoder *)aDecoder
{
    currentScore=0;
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        CGRect bounds = [self bounds];
        
        jumper = [[Jumper alloc] initWithFrame:CGRectMake(bounds.size.width/2, bounds.size.height - 20, 20, 20)];
        [jumper setBackgroundColor:[UIColor redColor]];
        [jumper setDx:0];
        [jumper setDy:10];
        [self addSubview:jumper];
        [self makeBricks:nil];
    }
    return self;
}

-(IBAction)makeBricks:(id)sender
{
    CGRect bounds = [self bounds];
    float width = bounds.size.width * .2;
    float height = 20;
    
    if (bricks)
    {
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
        }
    }
        
    bricks = [[NSMutableArray alloc] init];
    for (int i = 0; i < 15; ++i)
        {
            Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
            [b setBackgroundColor:[UIColor blueColor]];
            [self addSubview:b];
            [b setCenter:CGPointMake(rand() % (int)(bounds.size.width), rand() % (int)(bounds.size.height))];
            [bricks addObject:b];
        }
}

- (void) gameOverHandler {
    //game a game over label
    CGRect bounds = [self bounds];
    UILabel *gameOverLabel = [[UILabel alloc] initWithFrame:CGRectMake(bounds.size.width/2-50, bounds.size.height/2, 100, 25)];
    [gameOverLabel setText:@"GAME OVER!"];
    [UIView animateWithDuration:1
                     animations:^{
                         gameOverLabel.alpha=0;
                     }];
    
    gameOverLabel.adjustsFontSizeToFitWidth=YES;
    [self addSubview:gameOverLabel];
    for (UIView *i in self.subviews){
        if([i isKindOfClass:[UILabel class]]){
            UILabel * lbl = (UILabel*) i;
            if([lbl.text isEqualToString:@"GAME OVER!"])//wrong label, skip
                continue;
            currentScore=0;
            [lbl setText:[NSString stringWithFormat:@"%d", 0]];
            break;
        }
    }
}

-(void)arrange:(CADisplayLink *)sender
{
    //CFTimeInterval ts = [sender timestamp];
    
    CGRect bounds = [self bounds];
    
    // Apply gravity to the acceleration of the jumper
    [jumper setDy:[jumper dy] - .3];
    
    // Apply the tilt.  Limit maximum tilt to + or - 5
    [jumper setDx:[jumper dx] + tilt];
    if ([jumper dx] > 5)
        [jumper setDx:5];
    if ([jumper dx] < -5)
        [jumper setDx:-5];
    
    // Jumper moves in the direction of gravity
    CGPoint p = [jumper center];
    p.x += [jumper dx];
    p.y -= [jumper dy];
    
    //score increased by pixels moved
    if([jumper dy] >0) {
        for (UIView *i in self.subviews){
            if([i isKindOfClass:[UILabel class]]){
                UILabel * lbl = (UILabel*) i;
                if([lbl.text isEqualToString:@"GAME OVER!"])//wrong label, skip
                    continue;
                currentScore = currentScore+((int) ([jumper dy] + 0.5));
                [lbl setText:[NSString stringWithFormat:@"%ld", currentScore]];
                break;
            }
        }
    }
    if ([jumper dy] > 0){
        for (Brick *b in bricks) {
            //move brick down, if it has reached the bottom of the game
            if (b.center.y < bounds.size.height-11){
                CGPoint newCenter = b.center;
                newCenter.y += [jumper dy]+5;
                b.center = newCenter;
            }
            else {//if brick has reached bottom, send to top
                [b setCenter:CGPointMake(rand() % (int)(bounds.size.width), 50 -(arc4random()%10))];
                //change brick type
                b.alpha=1;
                [b createRandomType];
            }
        }
    }
    
    // If the jumper has fallen below the bottom of the screen,
    // add a positive velocity to move him
    if (p.y > bounds.size.height)
    {
            //gameover
            [self gameOverHandler];
            [jumper setDy:10];
            p.y = bounds.size.height;
    }
    
    // If we've gone past the top of the screen, wrap around
    if (p.y < 0)
        p.y += bounds.size.height;
    
    // If we have gone too far left, or too far right, wrap around
    if (p.x < 0)
        p.x += bounds.size.width;
    if (p.x > bounds.size.width)
        p.x -= bounds.size.width;
    
    // If we are moving down, and we touch a brick, we get
    // a jump to push us up.
    if ([jumper dy] < 0)
    {
        for (Brick *brick in bricks)
        {
            CGRect b = [brick frame];
            if (CGRectContainsPoint(b, p))
            {
                //landed on bouncable brick, double dy
                if ([brick.type  isEqualToString: @"bouncable"]){
                    [jumper setDy:20];
                    continue;
                }
                
                //hit breakable block
                if ([brick.type isEqualToString:@"breakable"]) {
                    if(brick.alpha == 0) //already hit breakable block
                        continue;
                    brick.alpha = 0;
                    //hit breakable block, slow down fall by 5
                    [jumper setDy:[jumper dy]+5];
                    continue;
                }
                //else hit a normal block
                [jumper setDy:10];
            }
        }
    }
    
    [jumper setCenter:p];
    // NSLog(@"Timestamp %f", ts);
}

@end
