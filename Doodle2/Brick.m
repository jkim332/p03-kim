//
//  Brick.m
//  Doodle2
//
//  Created by Patrick Madden on 2/5/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import "Brick.h"

@implementation Brick
@synthesize type;

-(void) createRandomType {
    NSInteger chance = (arc4random() %10);
    
    //20% chance of being breakable brick
    if (chance < 2) {
        type = @"breakable";
        [self setBackgroundColor:[UIColor redColor]];
        return;
    }
    
    //10% chance of being bouncable brick
    if (chance ==3) {
        type = @"bouncable";
        [self setBackgroundColor:[UIColor greenColor]];
        return;
    }
    [self setBackgroundColor:[UIColor blueColor]];
    type = @"nothing";
}

@end
