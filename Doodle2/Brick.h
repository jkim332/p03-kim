//
//  Brick.h
//  Doodle2
//
//  Created by Patrick Madden on 2/5/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Brick : UIView
@property (nonatomic, strong) IBOutlet NSString* type;
-(void) createRandomType;
@end
