//
//  ViewController.m
//  Doodle2
//
//  Created by Patrick Madden on 2/4/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic, strong) CADisplayLink *displayLink;
@end

@implementation ViewController

@synthesize bricksButton;
- (void)viewDidLoad {
    [_score setText:[NSString stringWithFormat:@"%d", 0]];
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _displayLink = [CADisplayLink displayLinkWithTarget:_gameView selector:@selector(arrange:)];
    [_displayLink setPreferredFramesPerSecond:30];
    [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    CGRect bounds = [[UIScreen mainScreen] bounds];
    float width = bounds.size.width * .2;
    float height = 20;
    
    
    //Create brick legend to know what brick does what
    //Green = bouncable
    //Red = breakable
    
    //brick legend is to the right of the "Brick!" button
    float xPosition = bricksButton.frame.origin.x + bricksButton.frame.size.width +5;
    float yPosition = bricksButton.frame.origin.y + _gameView.frame.origin.y;
    
    
    //breakable brick legend
    UIView *breakableBrickLegend = [[UIView alloc] initWithFrame:CGRectMake(xPosition, yPosition, width, height)];
    [breakableBrickLegend setBackgroundColor:[UIColor redColor]];
    [self.view addSubview:breakableBrickLegend];
    //UILabel to denote what type of brick
    UILabel *breakable = [[UILabel alloc] initWithFrame:CGRectMake(xPosition, yPosition, width, height)];
    [breakable setText:@"Breakable"];
    breakable.adjustsFontSizeToFitWidth=YES;
    [self.view addSubview:breakable];
    
    //bouncable brick legend
    UIView *bouncableBrickLegend = [[UIView alloc] initWithFrame:CGRectMake(xPosition+width, yPosition, width, height)];
    [bouncableBrickLegend setBackgroundColor:[UIColor greenColor]];
    [self.view addSubview:bouncableBrickLegend];
    //UILabel to denote what type of brick
    UILabel *bouncable = [[UILabel alloc] initWithFrame:CGRectMake(xPosition+width, yPosition, width, height)];
    [bouncable setText:@"Bouncable"];
    bouncable.adjustsFontSizeToFitWidth=YES;
    [self.view addSubview:bouncable];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)speedChange:(id)sender
{
    UISlider *s = (UISlider *)sender;
    // NSLog(@"tilt %f", (float)[s value]);
    [_gameView setTilt:(float)[s value]];
}


@end
